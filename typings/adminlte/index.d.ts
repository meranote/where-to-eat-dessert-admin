// Type definitions for Adminlte 2.4
// TypeScript Version: 2.8

/// <reference types="jquery"/>

type LayoutMethodList = 'fix' | 'fixSidebar';

interface LayoutMethodOption {
  slimscroll?: boolean;
  resetHeight?: boolean;
}

interface TreeWidgetOption {
  animationSpeed?: number;
  accordion?: boolean;
  followLink?: boolean;
  trigger?: string;
}

interface JQuery {
  layout(method?: LayoutMethodList): JQuery;
  tree(options?: TreeWidgetOption): JQuery;
}

declare module 'adminlte' {
}
