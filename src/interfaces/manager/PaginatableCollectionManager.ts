import { Manager } from '@/interfaces/manager/Manager';
import { Paginatable } from '@/interfaces/document/Paginatable';
import { PaginateOption } from './PaginateOption';
import { Document } from '@/models/documents/Document';

export interface PaginatableCollectionManager<T extends Document> extends Manager {
  /** @inheritDoc */
  get(key: string): Promise<null|T>;
  /** @inheritDoc */
  getAll(paginate?: PaginateOption): Promise<Paginatable<T>>;
  /** @inheritDoc */
  getBy(conditions: { [key: string]: any }, paginate?: PaginateOption): Promise<Paginatable<T>>;
  /** @inheritDoc */
  save(document: T): Promise<void>;
}
