import { Manager } from './Manager';

export interface CollectionManager<T> extends Manager {
  /**
   * @inheritDoc
   */
  getAll(): Promise<T>;
}
