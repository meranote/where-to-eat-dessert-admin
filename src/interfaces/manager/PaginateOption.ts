export interface PaginateOption {
  page: number;
  limit: number;
}
