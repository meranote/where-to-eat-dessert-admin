/**
 * Manager Type.
 * Base type of all data manager.
 * @author Chaniwat Seangchai (chaniwat.meranote@gmail.com)
 */
export interface Manager {
  /**
   * Get total size of documents in collections.
   */
  count(): Promise<number>;
  /**
   * Get documents by document key.
   */
  get(key: string): Promise<any>;
  /**
   * Get all documents in collection.
   */
  getAll(): Promise<any>;
  /**
   * Get documents in collection with conditions.
   */
  getBy(conditions: { [key: string]: any }): Promise<any>;
  /**
   * Save document to collection.
   */
  save(document: any): Promise<any>;
}
