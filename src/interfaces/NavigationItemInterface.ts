import { Vue } from 'vue-property-decorator';
import { VueConstructor } from 'vue';

export interface NavigationItemInterface {
  name?: string;
  title: string;
  subtitle?: string;
  component?: VueConstructor<Vue>;
  icon?: string;
  path?: string;
  exactActive?: boolean;
  tree?: NavigationItemInterface[];
}
