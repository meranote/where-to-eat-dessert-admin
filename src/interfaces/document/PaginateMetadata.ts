export const PAGINATE_ORDER_INDEX_KEY = '_ordered_index';

export interface PaginateMetadata {
  total: number;
  per_page: number;
  current_page: number;
  last_page: number;
  from: number;
  to: number;
}
