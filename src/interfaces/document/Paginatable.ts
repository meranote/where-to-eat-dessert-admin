import { PaginateMetadata } from '@/interfaces/document/PaginateMetadata';

export interface Paginatable<T> extends PaginateMetadata {
  data: null|T[];
}
