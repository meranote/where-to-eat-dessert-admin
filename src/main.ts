// Import dependencies
import Vue from 'vue';

// Import getFirebase
import { getFirebase } from './main-firebase';

// Import admin-lte and dependents
import 'bootstrap';
import 'fastclick';
import 'admin-lte';

// Import vuetable-2
import * as VueTable from 'vuetable-2';

// Import global styles
import '@/assets/scss/main.scss';

// Import Our app's components
import App from '@/App.vue';
import router from '@/router';
import store from '@/states';

// Disable production tip
Vue.config.productionTip = false;

// If getFirebase is initial, make vue application
if (getFirebase()) {
  // Register Vue Plugins
  Vue.use(VueTable);
  /* tslint:disable:no-var-requires */
  // Register Additional components for vuetable via importing
  require('./components/global-component');
  // Initialize vue
  new Vue({
    router,
    store,
    render: (h) => h(App),
  }).$mount('#app');
} else {
  console.error('Firebase is not loaded, preventing vue to initialize the application.');
}
