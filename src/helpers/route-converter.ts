import { NavigationItemInterface } from '@/interfaces/NavigationItemInterface';
import { RouteConfig } from 'vue-router';

export function navigationItemToRouteConfigConverter(
  item: NavigationItemInterface,
  rootPath?: string,
): RouteConfig {
  return {
    path: item.path.startsWith(rootPath) ? item.path.substring(rootPath.length) : item.path,
    name: item.name,
    component: item.component,
    meta: {
      title: item.title,
      subtitle: item.subtitle,
      exactActive: item.exactActive,
    },
  };
}

export function multipleNavigationItemToRouteConfigConverter(
  items: NavigationItemInterface[],
  rootPath?: string,
): RouteConfig[] {
  const routes: RouteConfig[] = [];
  for (const item of items) {
    if (item.tree) {
      routes.push(...multipleNavigationItemToRouteConfigConverter(item.tree, rootPath));
    } else {
      routes.push(navigationItemToRouteConfigConverter(item, rootPath));
    }
  }
  return routes;
}
