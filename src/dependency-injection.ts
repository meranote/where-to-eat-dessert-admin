import 'reflect-metadata';
import { Container as InversifyContainer, decorate, injectable } from 'inversify';
import getDecorators from 'inversify-inject-decorators';
import { Manager } from '@/interfaces/manager/Manager';

/**
 * Main IoC Container for Dependency-Injections.
 */
export const Container = new InversifyContainer();

/**
 * [Decorator] Bind class as Collection Manager
 */
export function BindCollectionManager(collection, identifierKey) {
  return (constructor: { new(...args): Manager }) => {
    decorate(injectable(), constructor);
    Container.bind(constructor).to(constructor.bind(constructor, collection, identifierKey)).inSingletonScope();
  };
}

const { lazyInject } = getDecorators(Container, false);
export { lazyInject };
