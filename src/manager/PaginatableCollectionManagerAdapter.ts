import { PaginatableCollectionManager } from '@/interfaces/manager/PaginatableCollectionManager';
import { Paginatable } from '@/interfaces/document/Paginatable';
import { PaginateOption } from '@/interfaces/manager/PaginateOption';
import * as firebase from 'firebase';
import { getFirebase } from '@/main-firebase';
import { injectable } from 'inversify';
import { PAGINATE_ORDER_INDEX_KEY } from '@/interfaces/document/PaginateMetadata';
import { Document } from '@/models/documents/Document';
import * as _ from 'lodash';

@injectable()
export abstract class PaginatableCollectionManagerAdapter<T extends Document>
  implements PaginatableCollectionManager<T> {
  protected ref: firebase.firestore.CollectionReference;

  constructor(target: string, protected identifierKey: string) {
    this.ref = getFirebase().firestore().collection(target);
  }

  /** @inheritDoc */
  public async count(): Promise<number> {
    return await this.ref.get().then((documents) => documents.size);
  }

  /** @inheritDoc */
  public async get(key: string): Promise<null | T> {
    return await this.ref.doc(key).get().then((document) => {
      if (document.exists) {
        const castDocument = document.data() as T;
        castDocument.__id = document.id;
        return castDocument;
      } else {
        return null;
      }
    });
  }

  /** @inheritDoc */
  public async getAll(paginate: PaginateOption = { page: 1, limit: 10 }): Promise<Paginatable<T>> {
    // Prepare query
    const data: T[] = [];
    const size = await this.count();
    // Execute query and push shops data
    await this.ref.orderBy(PAGINATE_ORDER_INDEX_KEY)
      .startAt(((paginate.page - 1) * paginate.limit) + 1)
      .limit(paginate.limit)
      .get().then((documents) => {
      documents.forEach((document) => {
        const castDocument = document.data() as T;
        castDocument.__id = document.id;
        data.push(castDocument);
      });
    });
    // Make paginatable of data and returned
    return this.makePaginatable(data, size, paginate);
  }

  /** @inheritDoc */
  public async getBy(conditions: { [key: string]: any }, paginate?: PaginateOption): Promise<Paginatable<T>> {
    return undefined;
  }

  /** @inheritDoc */
  public async save(document: T): Promise<void> {
    const data: any = _.cloneDeep(document);
    if (!document.__id) {
      document.__id = document[this.identifierKey].split(' ').join('-');
      data._ordered_index = await this.count() + 1;
      await this.ref.doc(document.__id).set(data);
    } else {
      delete data.__id;
      await this.ref.doc(document.__id).update(data);
    }
  }

  /**
   * Make paginatable from data.
   * @param {T[]} data
   * @param {number} totalDocumentCount
   * @param {PaginateOption} paginate
   * @returns {Paginatable<T>}
   */
  protected makePaginatable(data: T[], totalDocumentCount: number, paginate: PaginateOption): Paginatable<T> {
    return {
      total: totalDocumentCount,
      per_page: paginate.limit,
      current_page: paginate.page,
      last_page: Math.ceil(totalDocumentCount / paginate.limit),
      from: ((paginate.page - 1) * paginate.limit) + 1,
      to: (() => {
        if (paginate.page === Math.ceil(totalDocumentCount / paginate.limit)) {
          return totalDocumentCount;
        } else {
          return ((paginate.page - 1) * paginate.limit) + paginate.limit;
        }
      })(),
      data,
    } as Paginatable<T>;
  }
}
