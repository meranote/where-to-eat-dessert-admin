import { PaginatableCollectionManagerAdapter } from './PaginatableCollectionManagerAdapter';
import { ShopDocument } from '@/models/documents/ShopDocument';
import * as firebase from 'firebase';
import { getFirebase } from '@/main-firebase';
import { BindCollectionManager } from '@/dependency-injection';

@BindCollectionManager('shops', 'name')
export class ShopManager extends PaginatableCollectionManagerAdapter<ShopDocument> {
  /**
   * Upload profile picture for shop.
   */
  public uploadShopProfilePicture(
    shopName: string,
    file: File,
    onProgressChanged?: (state: firebase.storage.TaskState, progress: number) => void,
  ): Promise<string> {
    // TODO: Add mime checking only to picture
    let resolver, rejector;
    const promise = new Promise<string>((res, rej) => { resolver = res; rejector = rej; });

    const rootRef = getFirebase().storage().ref();
    const targetRef = rootRef.child(`images/shops/${shopName.split(' ').join('-')}`);
    const uploadTask = targetRef.put(file);

    uploadTask.on('state_changed', (snapshot: any) => {
      // Observe state change events such as progress, pause, and resume
      // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
      const progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
      if (onProgressChanged) {
        onProgressChanged(snapshot.state, progress);
      }
    }, (error) => {
      // Handle unsuccessful uploads
      rejector(error);
    }, () => {
      // Handle successful uploads on complete
      // For instance, get the download URL: https://firebasestorage.googleapis.com/...
      resolver(uploadTask.snapshot.downloadURL);
    });

    return promise;
  }
}
