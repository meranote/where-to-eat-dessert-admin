import { Field } from '@/models/fields/Field';

export type CoordinatesField = Field<Coordinates>;
