import { Field } from '@/models/fields/Field';

export type TagsField = Field<string[]>;
