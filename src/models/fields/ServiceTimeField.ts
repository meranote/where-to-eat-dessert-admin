import { Field } from '@/models/fields/Field';

interface ServiceTimeRange {
  opened: string;
  closed: string;
}

export interface ServiceTimeField extends Field<object> {
  0: ServiceTimeRange; // Sunday
  1: ServiceTimeRange; // Monday
  2: ServiceTimeRange; // Tuesday
  3: ServiceTimeRange; // Wednesday
  4: ServiceTimeRange; // Thursday
  5: ServiceTimeRange; // Friday
  6: ServiceTimeRange; // Saturday
}
