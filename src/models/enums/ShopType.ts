export const ShopType = {
  'cafe': 'Cafe',
  'bakery': 'Bakery',
  'ice-cream_shop': 'Ice-Cream Shop',
};
