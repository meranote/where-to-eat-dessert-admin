import { Document } from './Document';
import { MenuDocument } from 'src/models/documents/MenuDocument';
import { CoordinatesField } from '@/models/fields/CoordinatesField';
import { ServiceTimeField } from '../fields/ServiceTimeField';
import { TagsField } from '../fields/TagsField';

export interface ShopDocument extends Document {
  address: string;
  description: string;
  location: CoordinatesField;
  menu: MenuDocument[];
  name: string;
  service_time: ServiceTimeField;
  tags: TagsField;
  telephone: string;
  type: string;
  profile_picture?: string;
}
