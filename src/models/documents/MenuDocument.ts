import { Document } from './Document';
import { TagsField } from '../fields/TagsField';

export interface MenuDocument extends Document {
  description: string;
  name: string;
  price: number;
  tags: TagsField;
  type: string;
}
