import { Component, Vue } from 'vue-property-decorator';

@Component({
  computed: {
    currentUser() {
      return this.$store.getters['auth/user'];
    },
  },
})
export class AuthenticationMixins extends Vue {
  protected doLogout() {
    this.$store.dispatch('auth/logout')
      .then(() => {
        return this.$router.push({ name: 'login' });
      });
  }
}
