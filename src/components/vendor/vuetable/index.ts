import Vue from 'vue';

/** We globally register the components for vuetable. */
import CustomVuetablePagination from './CustomVuetablePagination.vue';

Vue.component('custom-vuetable-pagination', CustomVuetablePagination);
