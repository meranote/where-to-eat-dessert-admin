export const VueTableStyle = {
  tableClass: 'table table-striped table-bordered table-hover',
  ascendingIcon: 'glyphicon glyphicon-chevron-up',
  descendingIcon: 'glyphicon glyphicon-chevron-down',
  handleIcon: 'glyphicon glyphicon-menu-hamburger',
  renderIcon: (classes, options) => `<span class="${classes.join(' ')}"></span>`,
};

export const VueTablePaginationStyle = {
  wrapperClass: 'pagination pull-right',
  activeClass: 'btn-primary',
  disabledClass: 'disabled',
  pageClass: 'btn btn-border',
  linkClass: 'btn btn-border',
  icons: {
    first: '',
    prev: '',
    next: '',
    last: '',
  },
};
