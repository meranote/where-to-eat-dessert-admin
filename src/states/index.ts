import Vue from 'vue';
import Vuex from 'vuex';

// Import our's states
import { AuthState } from './AuthState';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    auth: AuthState,
  },
});
