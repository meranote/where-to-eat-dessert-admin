import { getFirebase } from '@/main-firebase';

/**
 * Reference for user information
 */
interface UserInformationType {
  uid: string;
  email: string;
}

/**
 * Authentication state.
 * @author Chaniwat Seangchai (chaniwat.meranote@gmail.com)
 */
export const AuthState = {
  namespaced: true,
  state: {
    user: undefined,
  },
  getters: {
    user(state) {
      return state.user;
    },
    isLogin(state) {
      return !!state.user;
    },
  },
  mutations: {
    updateUser(state, user: UserInformationType) {
      state.user = user;
    },
    clearUserState(state) {
      state.user = undefined;
    },
  },
  actions: {
    login({ commit }, { email, password }) {
      const auth = getFirebase().auth();
      // Before sign-in, clear user state
      commit('clearUserState');
      // Sign-in
      return auth.signInAndRetrieveDataWithEmailAndPassword(email, password);
    },
    logout({ commit, state }) {
      const auth = getFirebase().auth();
      // Before sign-out, clear user state
      commit('clearUserState');
      // Sign-out
      return auth.signOut();
    },
  },
};

/** For getFirebase real-time API */
import store from '@/states';

/**
 * On authentication state changed, update user state.
 * Use with getFirebase's authenticate persistence.
 */
getFirebase().auth().onAuthStateChanged((user) => {
  if (user) {
    store.commit('auth/updateUser', {
      uid: user.uid,
      email: user.email,
    } as UserInformationType);
  } else {
    store.commit('auth/updateUser', null);
  }
});
