import * as firebase from 'firebase';
import 'firebase/firestore';

let instance: firebase.app.App = null;

/**
 * Getter helper for getFirebase instances
 * @returns {firebase.app.App}
 */
export function getFirebase(): firebase.app.App {
  return instance;
}

/**
 * Check if feature is loaded in getFirebase.
 * @param {string[]} features
 */
function FirebaseFeatureCheck(features: string[]) {
  if (!instance) {
    throw new Error('Firebase is not initialize yet.');
  } else {
    return features.filter((feature) => typeof instance[feature] === 'function');
  }
}

try {
  // Initialize getFirebase
  instance = firebase.initializeApp({
    apiKey: 'AIzaSyCSNuGJiyKnB00rb4iNczEw1ZwP7uZQ2XI',
    authDomain: 'where-to-eat-dessert.firebaseapp.com',
    databaseURL: 'https://where-to-eat-dessert.firebaseio.com',
    projectId: 'where-to-eat-dessert',
    storageBucket: 'where-to-eat-dessert.appspot.com',
    messagingSenderId: '855938661804',
  });
  // Check getFirebase's feature is loaded
  const features = ['auth', 'firestore', 'messaging', 'storage'];
  console.log(`Firebase SDK loaded with ${FirebaseFeatureCheck(features).join(', ')}`);
} catch (e) {
  // Initial getFirebase failed
  console.error('Error loading the Firebase SDK.');
  console.error(e);
}
