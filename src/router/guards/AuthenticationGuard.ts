import Vue from 'vue';
import { RawLocation, Route } from 'vue-router/types/router';

import store from '@/states';

const skipAuthPaths = ['/login'];

/**
 * Authentication guard, check if user is authenticated.
 */
export async function AuthenticationGuard(
  to: Route,
  from: Route,
  next: (to?: RawLocation | false | ((vm: Vue) => any) | void) => void,
) {
  // If user state if undefined, wait until its have value (null or UserInformation)
  if (typeof store.getters['auth/user'] === 'undefined') {
    let unwatch = () => {};
    await Promise.all([
      new Promise((resolve) => {
        unwatch = store.watch((state: any) => {
            return state.auth.user;
          },
          (val, oldVal) => {
            if (typeof oldVal === 'undefined') {
              resolve(true);
            }
          });
      }),
    ]);
    // Call to stop watch value
    unwatch();
  }

  if (skipAuthPaths.indexOf(to.path) === -1 && !store.getters['auth/isLogin']) {
    // If not in skip paths and not authenticated, redirect to login
    return next({ name: 'login' });
  } else if (to.name === 'login' && store.getters['auth/isLogin']) {
    // If in login paths and authenticated, redirect to index
    return next({ path: '/' });
  }
  // Else, and authenticated, continue to the next guard
  return next();
}
