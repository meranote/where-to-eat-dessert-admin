import { RouteConfig } from 'vue-router';

// Import master components and sub-routes
import Master from '@/views/admin/Master.vue';
import { adminRoutes } from './admin/';

// Root application route
const rootRoutes: RouteConfig[] = [
  {
    path: '/',
    redirect: '/admin',
  },
  {
    path: '/admin',
    component: Master,
    children: adminRoutes,
  },
];

// Import authentication components
import Login from '@/views/Login.vue';

// Authentication route
const authRoutes: RouteConfig[] = [
  {
    path: '/login',
    name: 'login',
    component: Login,
  },
];

export const routes = rootRoutes.concat(authRoutes);
