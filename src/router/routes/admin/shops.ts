import { RouteConfig } from 'vue-router';
import { NavigationItemInterface } from '@/interfaces/NavigationItemInterface';
import { multipleNavigationItemToRouteConfigConverter } from '@/helpers/route-converter';

// Import admin components
import AddShop from '../../../views/admin/shop/AddShop.vue';
import EditShop from '../../../views/admin/shop/EditShop.vue';

// Declare shops navigation items
export const adminShopsNavigationItems: NavigationItemInterface[] = [
  {
    name: 'shop-add',
    title: 'Add Shop',
    subtitle: 'Add new shops',
    component: AddShop,
    path: '/admin/shops/add',
  },
  {
    name: 'shop-edit',
    title: 'Edit Shop',
    subtitle: 'Edit shop detail',
    component: EditShop,
    path: '/admin/shops/:__id/edit',
  },
];

// Declare admin routes
export const adminShopsRoutes: RouteConfig[] =
  multipleNavigationItemToRouteConfigConverter(adminShopsNavigationItems, '/admin/');
