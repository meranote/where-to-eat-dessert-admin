import { RouteConfig } from 'vue-router';
import { multipleNavigationItemToRouteConfigConverter } from '@/helpers/route-converter';
import { NavigationItemInterface } from '@/interfaces/NavigationItemInterface';

// Import admin components
import Dashboard from '@/views/admin/Dashboard.vue';
import Shop from '@/views/admin/shop/Shop.vue';
import Empty from '@/views/admin/Empty.vue';

// Declare navigation items
export const adminNavigationItems: NavigationItemInterface[] = [
  {
    name: 'dashboard',
    title: 'Dashboard',
    subtitle: 'Overall System',
    component: Dashboard,
    icon: 'fa fa-th',
    path: '/admin/',
    exactActive: true,
  },
  {
    name: 'shop',
    title: 'Shop',
    subtitle: 'Manage shops',
    component: Shop,
    icon: 'fa fa-th',
    path: '/admin/shops',
  },
  {
    name: 'menu',
    title: 'Menu',
    subtitle: 'Manage menus',
    component: Empty,
    icon: 'fa fa-th',
    path: '/admin/menus',
  },
  {
    name: 'recommendation',
    title: 'Recommendation',
    subtitle: 'Manage recommendations',
    component: Empty,
    icon: 'fa fa-th',
    path: '/admin/recommendations',
  },
  {
    name: 'promotion',
    title: 'Promotion',
    subtitle: 'Manage promotions',
    component: Empty,
    icon: 'fa fa-th',
    path: '/admin/promotions',
  },
  {
    name: 'review',
    title: 'Review',
    subtitle: 'Manage reviews',
    component: Empty,
    icon: 'fa fa-th',
    path: '/admin/reviews',
  },
];

// Declare admin routes
export const adminRoutes: RouteConfig[] =
  multipleNavigationItemToRouteConfigConverter(adminNavigationItems, '/admin/');

// Import Additional sub-routes
import { adminShopsRoutes } from './shops';

// Merge sub-routes
adminRoutes.push(...adminShopsRoutes);
