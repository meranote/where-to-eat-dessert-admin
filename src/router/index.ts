import Vue from 'vue';
import Router from 'vue-router';

// Import routes
import { routes } from './routes';

Vue.use(Router);

const router = new Router({
  routes,
  linkActiveClass: 'active',
  linkExactActiveClass: 'active',
});

// Import guards
import { AuthenticationGuard } from './guards/AuthenticationGuard';

// Register guards
router.beforeEach(AuthenticationGuard); // Authentication

export default router;
